// #![feature(plugin, decl_macro)]
// #![plugin(rocket_codegen)]

// #[macro_use] extern crate rocket;

// #[get("/<name>/<age>")]
// fn hello(name:String,age:u8)->String{
//     format!("Hello, {} year old named {}!",age,name)
// }

// fn main() {
//     rocket::ignite().mount("/hello",routes![hello]).launch();
// }


// #![feature(plugin)]
// #![plugin(rocket_codegen)]

// extern crate rocket;

// #[get("/")]
// fn index() -> &'static str {
//     "Hello, world!"
// }

// fn main() {
//     rocket::ignite().mount("/", routes![index]).launch();
// }


#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

#[get("/hello/<name>/<age>")]
fn hello(name: String, age: u8) -> String {
    format!("Hello, {} year old named {}!", age, name)
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

fn main() {
    rocket::ignite().mount("/", routes![index,hello]).launch();
}