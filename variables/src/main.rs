
// fn main() {
//     let mut x = 5;
//     println!("The value of x is: {}",x);
//     x=6;
//     println!("The value of x is: {}",x);
// }



// fn main(){
//     // let x=5;
//     // let x=x+1;
//     // let x=x*2;
//     let x=2.0;
//     let y: f32=3.0;
//     println!("The value of x is: {}",x);
//     println!("The value of x is: {}",y);
//     let guess: u32="42".parse().expect("Not a number!");
//     println!("The value of guess is: {}",guess);
// }

fn main(){
    let sum = 5+10;
    let difference = 95.5-4.3;
    let product =4*30;
    let quotient =56.7/32.2;
    let remainder =43%5;
    println!("The value of sum is: {}",sum);
    println!("The value of difference is: {}",difference);
    println!("The value of product is: {}",product);
    println!("The value of quotient is: {}",quotient);
    println!("The value of remainder is: {}",remainder);
    let t=true;
    let f:bool=false;
    println!("The value of t is: {}",t);
    println!("The value of f is: {}",f);
    let c='z';
    let z='ℤ';
    let heart_eyed_cat = '😻';
    println!("The value of c is: {}",c);
    println!("The value of z is: {}",z);
    println!("The value of heart_eyed_cat is: {}",heart_eyed_cat);
    
    let tup:(i32,f64,u8)=(500,6.4,1);
    let (tup_x,tup_y,tup_z)=tup;
    println!("The value of tup is: {},{},{}",tup_x,tup_y,tup_z);
    println!("The value of tup is: {},{},{}",tup.0,tup.1,tup.2);
    
    let array_a=[1,2,3,4,5];
    println!("The value of array_a[0] is: {}",array_a[0]);
}